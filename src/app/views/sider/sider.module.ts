import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoNgZorroAntdModule } from 'src/app/ng-zorro-antd/ng-zorro-antd.module';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';
import { SiderComponent } from './sider.component';
import { EditProjectModule } from '../project/edit-project/edit-project.module';
import { ShowProjectInformationModule } from '../project/show-project-information/show-project-information.module';


@NgModule({
  declarations: [SiderComponent],
  imports: [
    CommonModule,
    BrowserModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DemoNgZorroAntdModule,
    EditProjectModule,
    ShowProjectInformationModule
  ],
  exports: [
    SiderComponent
  ],
  providers: [GlobalAppService]
})
export class SiderModule { }
