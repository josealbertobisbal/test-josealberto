import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { VirtualTimeScheduler } from 'rxjs';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';

@Component({
  selector: 'app-sider',
  templateUrl: './sider.component.html',
  styleUrls: ['./sider.component.scss']
})
export class SiderComponent implements OnInit {
  size: NzButtonSize = 'small';

  projectEditingEnabled!: boolean;

  showProjectInformation: string;

  constructor(
    private globalAppService: GlobalAppService
  ) { }



  ngOnInit(): void {
    this.globalAppService.sendProjectEditingEnabled$.subscribe(projectEditingEnabled => {
      this.projectEditingEnabled = projectEditingEnabled;
    });

    this.globalAppService.sendShowProjectInformation$.subscribe(showProjectInformation => {
      this.showProjectInformation = showProjectInformation;
    });

    this.showProjectInformation = 'error';
  }



}