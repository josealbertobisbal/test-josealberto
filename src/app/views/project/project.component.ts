import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { IProject } from 'src/app/core/models/project.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  searchValue = '';
  visible = false;
  isVisible = false;
  editButton = false;
  dataProject: IProject[] = [];
  dataProjectFilter: IProject[] = [];
  quantityProjects = 0;
  isVisibleAddModal = false;
  projectEditingEnabled!: boolean;
  showProjectInformation: string;
  projectId: string;
  project: IProject;
  projectEdit: IProject;
  formProject: FormGroup;
  isVisibleDeleteModal = false;
  isVisibleOk = false;
  checkedDisabled = true;
  isVisibleEditModal = false;
  buttonChecked = [];
  listSerialNumber = [];
  idTemp = '';
  checked = false;
  indeterminate = false;
  setOfCheckedId = new Set<string>();
  listOfCurrentPageData: IProject[] = [];
  disabledButton = true;
  disabledButtonAdd = false;
  number: string;
  quantityProjets = 0;
  tenderSumValue = 0;
  bidsTotalValue = 0;
  filterStatus: string;
  formatterPercent = (value: number) => `${value} %`;
  parserPercent = (value: string) => value.replace(' %', '');
  formatterDollar = (value: number) => `$ ${value}`;
  parserDollar = (value: string) => value.replace('$ ', '');

  constructor(
    private globalAppService: GlobalAppService,
    private message: NzMessageService
  ) {
  }

  ngOnInit(): void {
    this.getAllProjets();
    this.globalAppService.filter$.subscribe(filter => {
      this.getAllProjets();
      this.filterStatus = filter;
    });

    this.globalAppService.projectCreated$.subscribe(projectCreated => {
      if (projectCreated) {
        this.dataProjectFilter = [...this.dataProjectFilter, projectCreated];
      }
    });

    this.isVisibleAddModal = false;
  }

  onCloced() {
    this.isVisibleAddModal = false;
  }

  getAllProjets(): void {
    this.globalAppService.getAllElements('getall')
      .subscribe(response => {
        this.dataProjectFilter = [...response];
        this.dataProject = [...response];
        if (this.filterStatus === 'Active') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Active');
        }
        if (this.filterStatus === 'Pre Bid') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Pre Bid');
        }
        if (this.filterStatus === 'Warranty') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Warranty');
        }
        if (this.filterStatus === 'Archived') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Archived');
        }
        if (this.filterStatus === 'Inactive') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Inactive');
        }
        if (this.filterStatus === 'Created') {
          this.dataProjectFilter = this.dataProjectFilter.filter(data => data.Status === 'Created');
        }
        this.quantityProjets = this.dataProjectFilter.length;
        console.log(this.dataProjectFilter);
      });
  }

  editProject() {
   // this.projectEditingEnabled = true;
    this.globalAppService.sendShowProjectInformation('editing');
    this.globalAppService.sendProject(this.project);
  }

  deleteProject() {
    console.log(this.idTemp);
    this.globalAppService.delete(this.idTemp, '_delete').subscribe((deleteProject: any) => {
      this.dataProjectFilter = this.dataProjectFilter.filter(d => d.ProjectId !== this.idTemp);
    });
    this.message.create("success", `Deleted  ${"correctly"}`);
    this.quantityProjects = this.quantityProjects - 1;
    this.disabledButton = true;
    this.disabledButtonAdd = false;
  }


  filter(): void {
    this.reset();
    this.search();
  }

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.visible = false;
    this.dataProject = this.dataProject.filter((item: IProject) => item.Number.indexOf(this.searchValue) !== -1);
  }


  showModalNewProject() {
    this.isVisibleAddModal = !this.isVisibleAddModal;

  }

  showModaDeleteProject(): void {
    this.isVisibleDeleteModal = true;
  }

  editHandleCancel(): void {
    this.isVisibleEditModal = false;
  }

  deletehandleOk(): void {
    this.deleteProject();
    this.isVisibleDeleteModal = false;
  }

  deletehandleCancel(): void {
    this.isVisibleDeleteModal = false;
  }



  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.ProjectId, value));
    this.refreshCheckedStatus();
  }

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }

  }

  onItemChecked(id: string, checked: boolean) {
    this.projectId = id;
    this.idTemp = id;

    console.log('idTemp: ' + this.idTemp);
    console.log('projectEditingEnabled: ' + this.projectEditingEnabled);
    console.log(this.setOfCheckedId);

    this.project = this.dataProject.find(project => project.ProjectId === id);
    this.setOfCheckedId.forEach(x => {
      if (x !== id) {
        this.setOfCheckedId.delete(x);
      }
    });
    if (checked) {
      this.globalAppService.sendProjectEditingEnabled(null);
      this.globalAppService.sendShowProjectInformation('showEditing');
      this.number = this.dataProject.find(project => project.ProjectId === id).Number;
      this.project = this.dataProject.find(project => project.ProjectId === id);
      this.globalAppService.sendProject(this.project);
      this.disabledButton = false;
      this.disabledButtonAdd = true;
      this.buttonChecked.push(id);
      console.log(this.buttonChecked);

    } else if (checked && this.dataProject.find(project => project.ProjectId === id) && this.projectEditingEnabled) {
     // this.globalAppService.sendShowProjectInformation(false);
      this.number = this.dataProject.find(project => project.ProjectId === id).Number;
      this.project = this.dataProject.find(project => project.ProjectId === id);
      this.globalAppService.sendProject(this.project);
      this.disabledButton = false;
      this.disabledButtonAdd = true;
      this.buttonChecked.push(id);
      console.log(this.buttonChecked);

    }else if (checked === false) {
      this.globalAppService.sendShowProjectInformation('error');
      this.projectEditingEnabled = checked;
      this.globalAppService.sendProjectEditingEnabled(this.projectEditingEnabled);
      this.disabledButtonAdd = false;
      this.disabledButton = true;
    }
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: IProject[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  disabledCheckedStatus(): void {

    this.checked = false;
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.ProjectId));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.ProjectId)) && !this.checked;
  }

}
