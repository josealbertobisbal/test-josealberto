import { Component, OnInit } from '@angular/core';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { IProject } from 'src/app/core/models/project.model';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';

@Component({
  selector: 'app-show-project-information',
  templateUrl: './show-project-information.component.html',
  styleUrls: ['./show-project-information.component.scss']
})
export class ShowProjectInformationComponent implements OnInit {
  project: IProject;
  size: NzButtonSize = 'default';
  constructor( private globalAppService: GlobalAppService,) { }

  ngOnInit(): void {
    this.globalAppService.project$.subscribe(project => {
      this.project = project;
    });
    }

}
