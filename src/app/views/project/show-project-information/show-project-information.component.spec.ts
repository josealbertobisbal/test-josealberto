import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowProjectInformationComponent } from './show-project-information.component';

describe('ShowProjectInformationComponent', () => {
  let component: ShowProjectInformationComponent;
  let fixture: ComponentFixture<ShowProjectInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowProjectInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowProjectInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
