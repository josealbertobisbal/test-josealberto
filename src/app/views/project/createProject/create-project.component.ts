import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { debounceTime, switchMap } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Observable, Observer } from 'rxjs';
import { IProject } from 'src/app/core/models/project.model';
import { IAddress } from 'src/app/core/models/address.model';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {
  formProject: FormGroup;
  formAddress: FormGroup;
  project: IProject = {
    CurrencyId: '892705a2-bec9-4f4a-8f33-6b9c3879d600',
    IsProject: true,
    Status: 'Active'
  };

  address: IAddress = {
    Active: true
  };
  projectCreated: IProject;
  @HostBinding('class.is-open')
  isVisibleAddModal = true;
  disabledSave = false;
  @Output() cloced: EventEmitter<void> = new EventEmitter<void>();
  @Input() dataProject: IProject[] = [];

  constructor(
    private globalAppService: GlobalAppService,
    private message: NzMessageService,
    private formBuilder: FormBuilder
  ) {

    this.buildFormProject();
    this.buildFormAddress();
  }

  ngOnInit(): void {

  }

  buildFormProject() {

    this.formProject = this.formBuilder.group({
      Status: 'Active',
      Number: ['', [Validators.required], [this.nroSerieAsyncValidator]],
      Name: ['', [Validators.required]],
      StartDate: ['', [Validators.required]],
      EndDate: ['', [Validators.required]],
      CurrencyId: '892705a2-bec9-4f4a-8f33-6b9c3879d600',
      IsProject: true
    });

    this.formProject.valueChanges.
    pipe(
      debounceTime(500)
    );

  }



  buildFormAddress() {

    this.formAddress = this.formBuilder.group({
      Line1: ['', [Validators.required]],
      Line2: [''],
      City: ['', [Validators.required]],
      Country: ['', [Validators.required]],
      State: [''],
      PostalCode: ['', [Validators.required]],
      Active: true
    });

    this.formAddress.valueChanges.
      pipe(
        debounceTime(500)
      );
  }


  nroSerieAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (this.dataProject.find(project => project.Number === control.value)) {
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
        observer.complete();
      }, 1000);
    });

  createProject() {
    
    this.address = this.formAddress.value;
    this.globalAppService.createAddress(this.address).subscribe(
      (newAddress: IAddress) => {
        this.project = this.formProject.value;
        this.project.AddressId = newAddress.AddressId;
        this.globalAppService.create(this.project, '_create').subscribe(
          newProject => {
            this.projectCreated = newProject;
            this.globalAppService.sendProjectCreated(this.projectCreated);
          }
        );
      });

    this.isVisibleAddModal = false;
    this.cloced.emit();
  }


  handleCancel(): void {
    this.isVisibleAddModal = false;
    this.cloced.emit();
  }

}
