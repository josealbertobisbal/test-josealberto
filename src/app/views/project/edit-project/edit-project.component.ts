import { Component, HostBinding, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { debounceTime } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable, Observer } from 'rxjs';
import { IProject } from 'src/app/core/models/project.model';
import { ProjectComponent } from '../project.component';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {
  project: IProject;
  formProject: FormGroup;
  dataProject: IProject[];
  isVisibleDeleteModal = false;
  isVisibleOk = false;
  tenderSumValue = 0;
  bidsTotalValue = 0;
  formatterPercent = (value: number) => `${value} %`;
  parserPercent = (value: string) => value.replace(' %', '');
  formatterDollar = (value: number) => `$ ${value}`;
  parserDollar = (value: string) => value.replace('$ ', '');

  constructor(
    private globalAppService: GlobalAppService,
   // private formBuilder: FormBuilder,
    private message: NzMessageService
  ) {
   // this.buildForm();
  }

  ngOnInit(): void {
    this.globalAppService.project$.subscribe(project => {
      this.project = project;
      console.log(this.project);
    });

  }


  numberAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (this.dataProject.find(project => project.Number === control.value)) {
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
        observer.complete();
      }, 1000);
    })

  /*
  editProject() {
    console.log('edit project');
    console.log(this.project);
    console.log(this.project.Status);
  }

*/

  editProject() {
    console.log('edit project');
    this.globalAppService.update(this.project, '_update').subscribe((newProject: any) => {
    });
    console.log(this.project);
    this.message.create('success', `Edited  ${'correctly'}`);
    //  this.quantityProjects = this.quantityProjects + 1;
  }

}
