import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project.component';
import { BrowserModule } from '@angular/platform-browser';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoNgZorroAntdModule } from 'src/app/ng-zorro-antd/ng-zorro-antd.module';
import { CreateProjectModule } from './createProject/create-project.module';
import { SiderModule } from '../sider/sider.module';
import { EditProjectModule } from './edit-project/edit-project.module';
import { FiltersModule } from '../filters/filters.module';



@NgModule({
  declarations: [ProjectComponent],
  imports: [
    CommonModule,
    BrowserModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DemoNgZorroAntdModule,
    ReactiveFormsModule,
    CreateProjectModule,
    CreateProjectModule,
    SiderModule,
    EditProjectModule,
    FiltersModule
  ],
  exports: [
    ProjectComponent
  ]
})
export class ProjectModule { }
