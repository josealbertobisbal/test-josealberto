import { Component, OnInit } from '@angular/core';
import { GlobalAppService } from 'src/app/shared/globalAppServices/global-app-services.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  filter: string;
  
  constructor(
    private globalAppService: GlobalAppService
  ) { }



  ngOnInit(): void {
  }



  tabClickAll() {
    this.filter = 'All';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickActive() {
    this.filter = 'Active';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickPrebid() {
    this.filter = 'Pre Bid';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickWarranty() {
    this.filter = 'Warranty';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickArchived() {
    this.filter = 'Archived';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickInactived() {
    this.filter = 'Inactive';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }

  tabClickCreated() {
    this.filter = 'Created';
    this.globalAppService.sendFilter(this.filter);
    console.log(this.filter);
  }




}
