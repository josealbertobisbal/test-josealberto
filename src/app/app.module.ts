import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { DemoNgZorroAntdModule } from './ng-zorro-antd/ng-zorro-antd.module';
import { ReactiveFormsModule} from '@angular/forms';
import { GlobalAppService } from './shared/globalAppServices/global-app-services.service';
import { CreateProjectModule } from './views/project/createProject/create-project.module';
import { ProjectModule } from './views/project/project.module';
import { SiderModule } from './views/sider/sider.module';
import { EditProjectModule } from './views/project/edit-project/edit-project.module';
import { FiltersModule } from './views/filters/filters.module';
import { ShowProjectInformationModule } from './views/project/show-project-information/show-project-information.module';


registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DemoNgZorroAntdModule,
    ReactiveFormsModule,
    CreateProjectModule,
    ProjectModule,
    SiderModule,
    EditProjectModule,
    FiltersModule,
    ShowProjectInformationModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, GlobalAppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
