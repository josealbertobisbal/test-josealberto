import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IAddress } from 'src/app/core/models/address.model';
import { IProject } from 'src/app/core/models/project.model';
import { environment } from 'src/environments/environment';
import { map, switchMap, takeUntil, filter, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GlobalAppService {

  projectEditingEnabled!: boolean;
  private sendProjectEditingEnabledSubject = new BehaviorSubject<boolean>(this.projectEditingEnabled);
  sendProjectEditingEnabled$ = this.sendProjectEditingEnabledSubject.asObservable();

  showProjectInformation!: string;
  private sendShowProjectInformationSubject = new BehaviorSubject<string>(this.showProjectInformation);
  sendShowProjectInformation$ = this.sendShowProjectInformationSubject.asObservable();


  filter!: string;
  private sendFilterSubject = new BehaviorSubject<string>(this.filter);
  filter$ = this.sendFilterSubject.asObservable();

  project!: IProject;
  private sendProjectSubject = new BehaviorSubject<IProject>(this.project);
  project$ = this.sendProjectSubject.asObservable();

  projectCreated!: IProject;
  private sendProjectCreatedSubject = new BehaviorSubject<IProject>(this.projectCreated);
  projectCreated$ = this.sendProjectCreatedSubject.asObservable();

  addressId = '';
  private sendAddressIdSubject = new BehaviorSubject<string>(this.addressId);
  addressId$ = this.sendAddressIdSubject.asObservable();

  projectCleand!: IProject;
  private sendProjectCleandSubject = new BehaviorSubject<IProject>(this.projectCleand);
  projectCleand$ = this.sendProjectCleandSubject.asObservable();




  constructor(private http: HttpClient) {}

  sendProjectEditingEnabled(projectEditingEnabled: boolean) {
    this.projectEditingEnabled = projectEditingEnabled;
    this.sendProjectEditingEnabledSubject.next(projectEditingEnabled);
  }

  sendShowProjectInformation(showProjectInformation: string) {
    this.showProjectInformation = showProjectInformation;
    this.sendShowProjectInformationSubject.next(showProjectInformation);
  }

  sendFilter(filter: string) {
    this.filter = filter;
    this.sendFilterSubject.next(filter);
  }

  sendProject(project: IProject) {
    this.project = project;
    this.sendProjectSubject.next(project);
  }

  sendProjectCreated(projectCreated: IProject) {
    this.projectCreated = projectCreated;
    this.sendProjectCreatedSubject.next(projectCreated);
  }

  sendAddressId(addressId: string) {
    this.addressId = addressId;
    this.sendAddressIdSubject.next(addressId);
  }

  sendProjectCleand(projectCleand: IProject) {
    this.projectCleand = projectCleand;
    this.sendProjectCleandSubject.next(projectCleand);
  }
//************Project services*****************//

  getAllElements(identifier: string): Observable<any> {
    const path = `${environment.apiUrlProject}${identifier}`;
    return this.http.get<any>(path);
  }

  getElementById(id: string, identifier: string): Observable<any> {
    const path = `${environment.apiUrlProject}${identifier}/${id}`;
    return this.http.get<any>(path);
  }

  create(model: any, identifier: string) {
    let json = JSON.stringify(model);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    const path = `${environment.apiUrlProject}${identifier}`;
    return this.http.post(path, json, { headers: headers });
  }


  update(model: any, identifier: string) {
    const path = `${environment.apiUrlProject}${identifier}`;
    return this.http.put(path, model);
  }


  delete(id: string, identifier: string) {
    const path = `${environment.apiUrlProject}${identifier}/${id}`;
    return this.http.delete(path);
  }


//************Address services*****************//

  createAddress(model: any) {
    let json = JSON.stringify(model);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    const path = `${environment.apiUrlAddress}${'create'}`;
    return this.http.post(path, json, { headers: headers });
  }

  getAddressById(id: string): Observable<any> {
    const path = `${environment.apiUrlAddress}/${id}`;
    return this.http.get<any>(path);
  }


  createProject(address: IAddress, project: IProject): void {
    this.createAddress(address)
      .pipe(switchMap(_address => {
        project.AddressId = address.AddressId;
        return this.create(project, 'project');
      }));
  }

}
