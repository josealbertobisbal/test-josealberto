export class IAddress {
    AddressId?: string;
    Line1?: string;
    Line2?: string;
    Country?: string;
    State?: string;
    County?: string;
    City?: string;
    PostalCode?: string;
    CreatedBy?: string;
    CreatedOn?: Date = new Date();
    ModifiedBy?: string;
    ModifiedOn?: Date = new Date();
    Active: boolean = true;
    AdditionalDetails?: string;
  }
